class playCube:
    value=0
    def __init__(self, cube_value=0):
        self.value = cube_value

    def set_value(self, value):
        self.value = value

    def get_value(self):
        return self.value

class theGrid:
    grid = []
    ROWS = 4
    COLUMNS = 4
    for i in range(ROWS):
        grid.append([] * COLUMNS)

    def __init__(self):
        for i in range(self.ROWS):
            for x in range(self.COLUMNS):
                self.grid[x].append(playCube())

    def displayTheGrid(self):
        for i in range(self.ROWS):
            output = ''
            for x in range(self.COLUMNS):
                output += str(self.grid[i][x].get_value()) + '  '
            print(output)

if __name__ == '__main__':
    game = theGrid()
    game.displayTheGrid()
